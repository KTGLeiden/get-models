﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace getmodels
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentDir = System.Environment.CurrentDirectory;
            var ext = ".model.ts";
            var myFiles = Directory.GetFiles(Path.Combine(currentDir, "test"), "*.*", SearchOption.AllDirectories)
                .Where(s => Path.GetFileName(s).Contains(ext));
            
            foreach (var pathToFile in myFiles)
            {
                foreach(var line in File.ReadAllLines(pathToFile)) {
                    Console.WriteLine(line);
                }
            }
        }
    }
}
